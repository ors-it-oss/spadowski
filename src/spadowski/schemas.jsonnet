local sanitize(schema_list) = std.uniq(std.sort(schema_list));
//local project_filters = sanitize(project_filters);
//local string_list = {
//	type: 'list',
//	schema: {
//		type: 'string'
//	}
//};
local str_list(allowed) = {
		type: 'list',
		schema: {
			type: 'string',
			allowed: allowed
		}
};

local keyed_strdict = {
	type: 'dict',
	keysrules:{
		type: 'string'
	}
};

function(attributes_all, attributes_update, pipeline_filters, project_filters) {
	local sane_project_filters = project_filters,

/*
********************
	 All-item schema's
********************
*/
project: {
	attributes: str_list(sanitize(attributes_all)),
},

/*
******************
		Roster jobspec
******************
*/
	roster: {
		attributes: keyed_strdict + { keysrules+: { allowed: sanitize(attributes_update) }},
		filters: keyed_strdict + { keysrules+: { allowed: sane_project_filters }},
		pipelines: {
			type: 'dict',
			keysrules: {
				type: 'string',
				allowed: sanitize(pipeline_filters)
			}
		},
		registry: {
			type: 'dict',
			schema:{
				keep_n: {	type: 'integer'	},
				older_than: { type: 'string' },
				name_regex: { type: 'string' },
			}
		}
	}

}
