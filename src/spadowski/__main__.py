#!/bin/python3
# -*- coding: UTF-8 -*-
'''
Spadowski is a Gitlab Janitor
'''
# Python
import collections
import os
import sys
import argparse
from argparse import ArgumentError

# 3rd
# import _jsonnet
import gitlab
import json
import prettytable
import requests
import yaml

# Own
import cerberus
import spadowski
from spadowski import Spadowski, ValidatorException

# Logging & Debugging
import logging
import pprint

ENV_PREFIX = 'SPADOWSKI'
log = logging.getLogger()
logging.basicConfig(
    level=logging.getLevelName(os.environ.get(f'{ENV_PREFIX}_LOGLEVEL', 'info').upper()),
    format='%(levelname)s %(message)s',
    handlers=[logging.StreamHandler(sys.stderr)]
)
ppr = pprint.PrettyPrinter(indent=2).pprint


def _kvsplitaction(validschema, key):
    class KVSplitAction(argparse.Action):
        '''
        -o arg1=val1 arg2=val2 > args.o = {arg1: val1, arg2: val2}
        See https://docs.python.org/3.7/library/argparse.html#action-classes
        '''
        def __call__(self, parser, namespace, values, option_string=None):
            try:
                # noinspection PyTypeChecker
                # fails to see .split(str, 1) delivers Iterable[Any, Any] not list[str]
                res = dict([kv.split('=', 1) for kv in values])
                validator = cerberus.Validator(validschema)
                valid = validator.validate({key: res})
                if not valid:
                    raise ArgumentError(self, validator.errors[key])
                setattr(namespace, self.dest, res)
            except ValueError:
                raise ArgumentError(self, 'Invalid key=value in ' + ' '.join([f'"{val}"' for val in values]))
    return KVSplitAction


def _listsplitaction(choices=None):
    class ListSplitAction(argparse.Action):
        '''
        -o arg1,arg2,arg3 > args.o = [arg1,arg2,arg3]
        '''
        def __call__(self, parser, namespace, values, option_string=None):
            try:
                res = values.split(',')
                if choices is not None:
                    for el in res:
                        if el not in choices:
                            raise ArgumentError(self, f'Invalid choice {el}')

                setattr(namespace, self.dest, res)
            except ValueError:
                raise ArgumentError(self, 'Invalid ,-separated list in ' + ' '.join([f'"{val}"' for val in values]))
    return ListSplitAction


ARGS = [
    dict(args=['-H', '--host'], action='store', desc='Gitlab server', required=False, env='GITLAB'),
    dict(args=['-T', '--token'], action='store', desc='Gitlab token', required=False, env='TOKEN')
]

COMMAND_CHOICES = {
    'attribute_set': ['none'] + [const[len('ATTRSET_'):].lower() for const in dir(spadowski.metadata) if const.startswith('ATTRSET_')],
    'attributes': Spadowski.schemas.get('project')['attributes']['schema']['allowed']
}

COMMANDS = {
    'roster': {
        'help': 'Perform a jsonnet duty roster',
        'args': [
            dict(args=['rosterfile'], type=argparse.FileType('r'), action='store', desc='Roster jsonnet'),
            dict(args=['-n', '--dry'], env='DRY', action='store_true', desc='Do not actually do anything')
        ]
    },
    'schema': {
        'help': 'Output roster schema (dynamically generated with Gitlab host)',
        'args': [
            dict(args=['schema_name'], action='store', default='*', choices=[ '*', 'compare' ] + sorted(Spadowski.schemas.all().keys()), desc='Roster to output'),
        ]
    },
    'search': {
        'help': 'Practice the filters',
        'args': [
            dict(args=['-r', '--rosterfile'], type=argparse.FileType('r'), action='store', desc='Use filters from Spadowski Roster'),
            dict(args=['filters'],
                 action=_kvsplitaction(Spadowski.schemas.get('roster'), 'filters'), nargs=argparse.REMAINDER,
                 desc='Search filters param1=value1 param2=value2, valid parameters: %s' % ', '.join(Spadowski.schemas.get('roster')['filters']['keysrules']['allowed'])),
            dict(args=['-a', '--attributes'], action=_listsplitaction(COMMAND_CHOICES['attributes'],),
                 desc='comma-separated list of attributes to return, valid attributes: '
                      f"{', '.join(COMMAND_CHOICES['attributes'])}"),
            dict(args=['-s', '--attribute_set'], action=_listsplitaction(COMMAND_CHOICES['attribute_set']),
                 desc='(comma-separated list of) well-known attributeset(s) to return, known sets: '
                      f"{', '.join(COMMAND_CHOICES['attribute_set'])}"),
            dict(args=['-o', '--output'], action='store', choices=['json', 'report', 'yaml', 'table'], default='yaml',
                 desc='Output format', ),
        ]
    },
    'version': {
        'args': [dict(args=['-o', '--online'], action='store_true', desc='Query Gitlab server too')],
        'help': 'Output version'
    }
}


def cli() -> argparse.ArgumentParser:
    '''
    Command line options.
    '''
    def _add_args(_parser: argparse.ArgumentParser, arg_dicts: list) -> None:
        arg_dict: dict
        for arg_dict in arg_dicts:
            help_txt = arg_dict.pop('desc', False)
            env = arg_dict.pop('env', False)
            if env:
                env = f'{ENV_PREFIX}_{env}'
                help_txt = f'{env}=) {help_txt}'
                default = os.environ.get(env, arg_dict.pop('default', False))
                if default:
                    arg_dict['default'] = default
                    arg_dict.pop('required', None)
            arg_dict['help'] = help_txt
            _parser.add_argument(*arg_dict.pop('args'), **arg_dict)

    parser = argparse.ArgumentParser(
        prog='spadowski',
        description='GitLab Janitor',
        formatter_class=argparse.RawDescriptionHelpFormatter)
    _add_args(parser, ARGS)

    parser_commands = parser.add_subparsers(title='Commands', dest='command', required=True)
    for cmd, argdata in COMMANDS.items():
        _args = argdata.pop('args', {})
        parser_cmd = parser_commands.add_parser(cmd, **argdata)
        _add_args(parser_cmd, _args)

    return parser


'''
Commands
'''


def roster():
    rosterd = spadowski.jnet_load(args.rosterfile.name)
    try:
        for jobspec in rosterd:
            janitor.plan(jobspec)
    except (RuntimeError, ValidatorException) as wrong:
        _except(wrong)


def schema():
    if args.schema_name == 'compare':
        gl_attr = set()
        for proj in janitor.search_projects(membership=True, starred=True):
            gl_attr.update(proj.attributes.keys())

        schema_attr = set(Spadowski.schemas.get('project')['attributes']['schema']['allowed'])

        orphan_server = gl_attr - schema_attr
        orphan_api = schema_attr - gl_attr
        if not orphan_api and not orphan_server:
            log.info('Schema and Gitlab are in perfect harmony')
            return True
        if orphan_server:
            log.info('Attributes found in projects but not in schema all attributes')
            log.info(json.dumps(sorted(orphan_server)))
        if orphan_api:
            log.info('Attributes found in schema but not on server')
            log.info(json.dumps(sorted(orphan_api)))

    elif args.schema_name == '*':
        print(yaml.safe_dump(Spadowski.schemas.all()))
    else:
        print(yaml.safe_dump(Spadowski.schemas.get(args.schema_name)))


def _search_table(proj_table, ret_attr):
    res = prettytable.PrettyTable()
    res.field_names = ('project',) + ret_attr
    res.align['project'] = 'l'

    for proj_k in sorted(proj_table.keys()):
        proj = proj_table[proj_k]
        rec = [proj_k]
        for k in res.field_names[1:]:
            v = proj.get(k, None)

            if isinstance(v, dict):
                vt = prettytable.PrettyTable()
                vt.field_names = ['k', 'v']
                vt.set_style(prettytable.PLAIN_COLUMNS)
                vt.align = 'l'
                vt.header = False
                vt.padding_width = 0
                vt.add_rows([(k, v[k]) for k in sorted(v.keys())])
                v = vt.get_string()
            elif isinstance(v, (list, tuple)):
                v = ','.join(v)
            elif k.endswith('_access_level') and v == 'enabled':
                v = '\U0001F4E2'  # loudspeaker
            else:
                v = {
                    'enabled': '\U0001F7E2',  # Green circle
                    'disabled': '\U00002B55',  # hollow red circle
                    None: '',
                    'none': '',
                    'finished': '\N{chequered flag}',
                    True: '\U00002705',  # check mark button
                    False: '\U00002B1C',  # cross mark button
                    'public': '\U0001F4E2',  # loudspeaker
                    'private': '\U0001F512'  # lock
                }.get(v, v)
            rec.append(v)
        res.add_row(rec)
    return res.get_string()


def search():
    ret_attr = []
    if args.attribute_set == ['none']:
        args.attribute_set = []
    elif not args.attribute_set and not args.attributes:
        args.attribute_set = ['default']

    for aset in args.attribute_set:
        ret_attr += [attr for attr in getattr(spadowski.metadata, f"ATTRSET_{aset.upper()}") if attr not in ret_attr]
    if args.attributes:
        ret_attr += [attr for attr in args.attributes if attr not in ret_attr]
    ret_attr = tuple(ret_attr)

    try:
        janitor.validate({'attributes': ret_attr}, 'project')
        janitor.validate({'filters': args.filters}, 'roster')
    except ValidatorException as wrong:
        _except(wrong)

    filters = args.filters or {}
    if args.rosterfile:
        filters.update(spadowski.jnet_load(args.rosterfile.name)[0]['filters'])

    proj_table = {}
    proj_tree = {}
    for proj in janitor.search_projects(**filters):
        log.debug(f'Found {proj.path_with_namespace}')
        pns = proj.namespace['full_path']

        branch = proj_tree
        for nsk in pns.split('/'):
            if nsk not in branch:
                branch[nsk] = {}
            branch = branch[nsk]
        attributes = spadowski.filterdict(lambda el: el[0] in ret_attr, proj.attributes)
        branch[proj.name] = attributes
        proj_table[proj.path_with_namespace] = attributes

    if not proj_table:
        log.info('No results')
        return None
    elif args.output == 'json':
        res = json.dumps(proj_tree)
    elif args.output == 'report':
        pass
    elif args.output == 'table':
        res = _search_table(proj_table, ret_attr)

    elif args.output == 'yaml':
        res = yaml.safe_dump(proj_tree)

    print(res)


def version():
    print(f'Cleaning up server {janitor.gl.url}: v{janitor.gl.version()[0]}')


'''
Execution
'''
janitor = None
args = None


def _except(wrong):
    message = {
        AssertionError.__qualname__: f'Assertion error: {wrong}',
        gitlab.exceptions.GitlabAuthenticationError.__qualname__: f'Authentication error: {wrong}',
        requests.exceptions.ConnectionError.__qualname__: 'Connecting to {args.host} failed: {wrong.args}',
    }.get(wrong.__class__.__qualname__, wrong)
    log.error(message)
    if log.level == logging.DEBUG:
        raise wrong
    sys.exit(1)


def main():
    global args, janitor
    args = cli().parse_args()
    if args.command == 'version':
        print(f'Spadowski the Gitlab janitor: v{spadowski.__version__}')
        if not getattr(args, 'online', False):
            sys.exit(0)

    try:
        janitor = Spadowski(args.host, args.token)
        if getattr(args, 'dry', False):
            janitor.dry = True
        globals()[args.command]()
    except KeyboardInterrupt:
        log.warning('Interrupted by your keyboard')
        sys.exit(2)
    except (AssertionError,
            gitlab.exceptions.GitlabAuthenticationError,
            requests.exceptions.ConnectionError) as wrong:
        _except(wrong)


if __name__ == "__main__":
    main()
