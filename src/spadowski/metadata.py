import os
import pathlib

# with open(os.path.join(pathlib.Path(__file__).parent.absolute(), 'VERSION')) as _fh:
#     __version__ = _fh.read().strip()

__author__ = "The Loeki"
__project__ = "Spadowski, the Gitlab Janitor"

ATTRSET_FEATURES = (
    'auto_devops_enabled',
    'ci_forward_deployment_enabled',
    'container_registry_enabled',
    'emails_disabled',
    'jobs_enabled',
    'lfs_enabled',
    'packages_enabled',
    'printing_merge_request_link_enabled',
    'request_access_enabled',
    'requirements_enabled',
    'security_and_compliance_enabled',
    'service_desk_enabled',
    'shared_runners_enabled',
)

ATTRSET_ACCESS_RW = (
    'analytics_access_level',
    'builds_access_level',
    'forking_access_level',
    'issues_access_level',
    'merge_requests_access_level',
    'operations_access_level',
    'pages_access_level',
    'repository_access_level',
    'snippets_access_level',
    'wiki_access_level',
    'restrict_user_defined_variables',
)

ATTRSET_ACCESS_RO = (
    'public_jobs',
    'shared_with_groups',
    'permissions'
)

ATTRSET_ACCESS = ATTRSET_ACCESS_RW + ATTRSET_ACCESS_RO

ATTRSET_DEFAULT = (
                      'visibility',
                      'archived',
                      'compliance_frameworks',
                  ) + ATTRSET_FEATURES + ATTRSET_ACCESS

ATTR_UPDATE_XTRA = (
                       # 'compliance_frameworks',  # Can't get it to update
                       'container_expiration_policy',
                       # 'public_jobs', TODO: Probably GET public_jobs POST public_builds
                   ) + ATTRSET_ACCESS_RW + ATTRSET_FEATURES

# Last compared w/gitlab.com 23-03-2021
NOT_IN_API = ('_links', 'allow_merge_on_skipped_pipeline', 'avatar_url', 'can_create_merge_request_in',
              'ci_forward_deployment_enabled', 'container_expiration_policy', 'container_registry_image_prefix',
              'created_at', 'creator_id', 'empty_repo', 'forks_count', 'http_url_to_repo', 'id', 'import_status',
              'issues_template', 'last_activity_at', 'marked_for_deletion_at', 'marked_for_deletion_on',
              'merge_requests_template', 'name_with_namespace', 'namespace', 'open_issues_count', 'path_with_namespace',
              'permissions', 'readme_url', 'service_desk_address', 'shared_with_groups', 'ssh_url_to_repo',
              'star_count', 'statistics', 'web_url')

NOT_IN_SERVER = ['avatar', 'build_git_strategy', 'container_expiration_policy_attributes',
                 'group_with_project_templates_id', 'import_url', 'initialize_with_readme',
                 'mirror_overwrites_diverged_branches', 'mirror_trigger_builds', 'mirror_user_id', 'namespace_id',
                 'only_mirror_protected_branches', 'public_builds', 'repository_storage', 'template_name',
                 'template_project_id', 'use_custom_template']
