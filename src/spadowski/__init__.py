# -*- coding: UTF-8 -*-
"""
Spadowksi is a Gitlab Janitor

"""

# from .metadata import (
#     __version__
# )

from .version import version as __version__

# Python
import collections
import json
import os
import pathlib
# import sys
from decimal import Decimal
from typing import Any, Callable

# 3rd party
import _jsonnet
import cerberus
import gitlab
import gitlab.v4.objects
import yaml

# Own
import spadowski
import spadowski.metadata

# Logging & Debugging
import logging
import pprint

log = logging.getLogger(__name__)
ppr = pprint.PrettyPrinter(indent=2).pprint


def filterdict(_filtercb: Callable[[Any, Any], bool], srcdict: dict, both=False) -> dict:
    '''
    Filter a dict based on a callback

    :param _filtercb: callback function taking k,v and returning True/False
    :param srcdict: dict to filter
    :param both: return both sets (Trues & Falses)
    :return: one or two dictionaries
    '''
    if both:
        ayes = {}
        nayes = {}
        for _el in srcdict.items():
            if _filtercb(_el):
                ayes[_el[0]] = _el[1]
            else:
                nayes[_el[0]] = _el[1]
        return ayes, nayes
    else:
        return dict(filter(_filtercb, srcdict.items()))


def jnet_load(jnet, vars={}, **kwargs) -> dict:
    '''
    Render stuff through JSONnet

    :param jnet: Snippet or file
    :param vars: Passed on to JSONnet as External Variables
    :param kwargs: Passed on to JSONnet as Top-Level Arguments
    :return: JSONnet generated object
    '''
    for d in kwargs, vars:
        for k, v in d.items():
            d[k] = json.dumps(v)

    if os.path.isfile(pathlib.Path(jnet).resolve()):
        jnobj = _jsonnet.evaluate_file(
            jnet, ext_vars=vars, tla_codes=kwargs
        )
    else:
        jnobj = _jsonnet.evaluate_snippet(
            'snippet', jnet, ext_vars=vars, tla_codes=kwargs
        )
    return json.loads(jnobj)


# class Validator(cerberus.Validator):
#     types_mapping = cerberus.Validator.types_mapping.copy()
#     types_mapping['decimal'] = cerberus.TypeDefinition('decimal', (Decimal,), ())


class Spadowski(object):
    '''
    Spadowski, the Gitlab janitor

    A Spadowski runs a roster against a gitlab to perform maintenance on a list of projects
    '''
    _filters = ('group',)
    _pipelines = ('keep_n', 'older_than')

    schemas = cerberus.schema.SchemaRegistry(jnet_load(
        os.path.join(pathlib.Path(__file__).parent.absolute(), 'schemas.jsonnet'),
        project_filters=gitlab.v4.objects.ProjectManager._list_filters + _filters,
        attributes_update=gitlab.v4.objects.ProjectManager._update_attrs[1] + spadowski.metadata.ATTR_UPDATE_XTRA,
        pipeline_filters=gitlab.v4.objects.ProjectPipelineManager._list_filters + _pipelines,
        attributes_all=list(
            set(gitlab.v4.objects.ProjectManager._update_attrs[1] +
                gitlab.v4.objects.ProjectManager._create_attrs[1] +
                spadowski.metadata.ATTRSET_DEFAULT +
                spadowski.metadata.NOT_IN_API) -
            {'issues_enabled', 'merge_requests_enabled', 'snippets_enabled', 'wiki_enabled'})  # Deprecated ones
    ))

    dry = False
    _roster = None

    def __init__(self, host: str, token: str, gl: gitlab.Gitlab = None):
        '''
        Initialize a connection to Gitlab and schema

        :param host:
        :param token:
        '''
        if not gl:
            assert host is not None, 'Need host'
            assert token is not None, 'Need token'
            if not host.startswith('http'):
                host = f'https://{host}'
            log.debug('Connecting to %s...', host)
            gl = gitlab.Gitlab(host, token, timeout=10, per_page=10)
            log.debug('Authing...')
            gl.auth()
        self.gl = gl
        self.validator = {}
        for sid, schema in self.schemas.all().items():
            self.validator[sid] = cerberus.Validator(schema)

    def plan(self, roster):
        '''
        Plan a single set of activities against a single group filter

        :param roster: dict containing roster
        :return:
        '''
        log.info('Planning the roster...')
        log.debug(roster)
        self.validate(roster, 'roster')
        self._roster = roster.copy()
        for project in self.search_projects(**roster.pop('filters')):
            log.info('Planning {}...'.format(project.path_with_namespace))
            for jobspec in roster.keys():
                getattr(self, jobspec)(project)
        log.info('Roster done')

    def search_projects(self, **filters: dict):
        '''
        Return a list of filter-matching projects on the Gitlab server

        :param filters: any of Gitlab's supported parameters or 'group'
                (run a schema to see what is possible)
        :return:
        '''
        filter_py, filter_api = filterdict(lambda k: k[0] in self._filters, filters, both=True)
        log.debug(f'Searching for {filter_api} post-treating with {filter_py}')
        checks = [{
                      'group': lambda p: p.namespace['full_path'] == v
                  }[k] for k, v in filter_py.items()]

        # TODO: Seems (2021.03) order_by is broken || yield doesnt do that)
        log.info(f'Querying {self.gl.url}...')
        for project in self.gl.projects.list(as_list=False, statistics=True, order_by='path', sort='asc', **filter_api):
            if all([check(project) for check in checks]):
                yield project

    def validate(self, data: dict, schema: str):
        res = self.validator[schema].validate(data)
        if not res:
            raise ValidatorException(self.validator[schema].errors)
        return True

    def attributes(self, project):
        '''
        correct attr

        :param project:
        '''
        chores = filterdict(lambda el: project.attributes.get(el[0], None) != el[1], self._roster['attributes'])
        chore_repr = ', '.join([f'{k}=>{v}' for k, v in chores.items()])
        if not chores:
            return
        elif self.dry:
            log.info(f'{project.path_with_namespace}: Attributes to update: {chore_repr}')
            return

        log.debug(f'{project.path_with_namespace}: Updating attributes with {chore_repr}')
        for k, v in chores.items():
            setattr(project, k, v)
        project.save()

    def pipelines(self, project):
        if not project.jobs_enabled:
            return
        chores = self._roster['pipelines']
        # chores = {k: v for k, v in chores.items() if k in self._pipelines}
        # chores = dict(filter(lambda k: k in self._pipelines, chores.items()))
        # pipeline_filters = {k: v for k, v in chores.items() if k not in self._pipelines}
        # chores = filtered_dict(lambda k: k in self._pipelines, chores)

        chores, pipeline_filters = filterdict(lambda el: el[0] in self._pipelines, chores, both=True)
        pipelines = project.pipelines.list(**pipeline_filters, as_list=False, sort='asc')

        if pipelines.total < chores.get('keep_n', pipelines.total):
            del chores['keep_n']

        i = pipelines.total
        for pipeline in pipelines:
            dead_pipe = False
            for k, v in chores.copy().items():
                if k == 'keep_n':
                    if i > v:
                        i -= 1
                        dead_pipe = True
                    else:
                        del chores['keep_n']

                # elif k == 'older_than':
                #
                #     date_cutoff = date_backup - datetime.timedelta(days=retention)
                #     ppr(pipeline.attributes)

            if dead_pipe:
                if self.dry:
                    log.info('Pipeline #{} ({}) to remove'.format(pipeline.id, pipeline.url))
                else:
                    log.debug('Deleting pipeline #{} ({})'.format(pipeline.id, pipeline.web_url))
                    pipeline.delete()

            if not chores:
                break

        #
        #
        # if pipelines.total <= v:
        #     continue
        #
        # q = v // self.gl.per_page
        # r = v % self.gl.per_page
        #
        # ppr('p: {} r: {} v: {} d: {}'.format(q,r,v, self.gl.per_page))
        #
        # for pipeline in project.pipelines.list(**pipeline_filters, as_list=False, page=q):
        #     ppr(pipeline.attributes)
        #     if r != 0:
        #         log.warning('IM NOW AT {}'.format(r))
        #         r -= 1
        #         continue
        #
        #     if self.dry:
        #         log.info('Pipeline #{} ({}) to remove'.format(pipeline.id, pipeline.url))
        #
        #     log.info('Pipeline #{} ({}) to remove'.format(pipeline.id, pipeline.web_url))

        # ppr(pipelines)

    def registry(self, project):
        if not project.container_registry_enabled:
            return

        chores = self._roster['registry']
        for repo in project.repositories.list(as_list=False):
            log.info('Cleaning up registry %s...', repo.name)
            if self.dry:
                log.debug('Repo {} has tags {}'.format(
                    repo.name, ','.join(sorted([repo.name for repo in repo.tags.list()])))
                )
                continue

            try:
                repo.tags.delete_in_bulk(**chores)
            except gitlab.exceptions.GitlabDeleteError as wrong:
                if wrong.response_code == 400:
                    continue
                raise wrong


class ValidatorException(ValueError):
    def __init__(self, errors, message='Validation failed'):
        self.errors = errors
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return '\n'.join([self.message, yaml.safe_dump(self.errors)])
