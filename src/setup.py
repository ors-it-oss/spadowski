# -*- coding: UTF-8 -*-
import setuptools

# metadata = {}
# with open("spadowski/metadata.py") as fp:
#     exec(fp.read(), metadata)

with open('requirements.txt') as _fh:
    install_requires = _fh.read().splitlines()

setuptools.setup(
    install_requires=install_requires,
)
