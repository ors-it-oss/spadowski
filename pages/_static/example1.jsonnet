[{
	filters: {
		membership: true,
		search: "oci"
	},
	pipelines: {
		older_than: "10d"
	}
},{
	filters: {
		membership: true,
		archived: true
	},
	attributes: {
		shared_runners_enabled: false,
    wiki_enabled: false
	}
}
]
