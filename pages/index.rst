Welcome to Spadowski, the Gitlab janitor's documentation!
=========================================================
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage
   roster

.. toctree::
   :maxdepth: 1
   :caption: Reference:

   cli
   schemas
   python

.. ss
   Reference
   =========
   * :ref:`genindex`
    ss * :ref:`modindex`
