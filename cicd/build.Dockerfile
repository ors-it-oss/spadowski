ARG BUILD_FROM=docker.io/debian:buster-slim
ARG FROM_SCRIPTS_TAG=latest
FROM registry.gitlab.com/ors-it-oss/oci-base/scripts:${FROM_SCRIPTS_TAG} AS scriptsrc
FROM $BUILD_FROM
LABEL \
  org.opencontainers.image.title="Spadowski development" \
  org.opencontainers.image.description="Container with Python env for Spadowski"

COPY --from=scriptsrc /include/common /include/apt /
COPY --from=scriptsrc \
  /include/install/usr/libexec/RUN.d/github.sh \
  /include/install/usr/libexec/RUN.d/google.sh \
  /usr/libexec/RUN.d/

#TODO: Crashes the bloody bugger
#COPY --from=scriptsrc include/python /
#https://github.com/pypa/pip/pull/5884
#ENV PIP_NO_CACHE_DIR=1

# Debian sh == dash <ugh not even [[ workx>
RUN /usr/libexec/RUN.sh fix_debconf &&\
	echo 'deb http://deb.debian.org/debian buster-backports main' >>/etc/apt/sources.list &&\
	apt update &&\
	apt -y full-upgrade &&\
	jsonnet_compile="make g++ python3-dev" &&\
	apt -y install $jsonnet_compile bash ca-certificates curl git jq less python3 python3-distutils python3-venv &&\
	apt-get clean &&\
	update-ca-certificates

RUN /usr/libexec/RUN.sh jsonnet_install

RUN \
	curl -L https://bootstrap.pypa.io/get-pip.py | python3 &&\
	ln -sf /usr/bin/python3 /usr/bin/python &&\
	ln -sf /usr/bin/pip3 /usr/bin/pip

ARG FAT
RUN \
	[ -n "$FAT" ] && pips="-r /build/src/requirements.txt";\
	pip --no-cache-dir install --upgrade build setuptools-scm pip jsonnet -r /build/pages/requirements.txt $pips

#ENV \
#	TWINE_PASSWORD \
#	TWINE_USERNAME
