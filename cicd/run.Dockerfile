ARG RUN_FROM=gcr.io/distroless/python3-debian10:latest

FROM $RUN_FROM
LABEL \
  org.opencontainers.image.title="Spadowski runtime container" \
  org.opencontainers.image.description="Containerised Spadowski"
COPY build/spadowski /spadowski
#RUN /spadowski/bin/python -m compileall

#ENV \
#	SPADOWSKI_DRY= \
#	SPADOWSKI_GITLAB= \
#  SPADOWSKI_LOGLEVEL= \
#  SPADOWSKI_TOKEN= \
#  SPADOWSKI_ROSTER=

ENTRYPOINT ["/spadowski/bin/spadowski"]
