#!/bin/bash
(echo "$VERBOSE" | grep -q -e "all" -e "shell") && set -x
set -e -o pipefail

####---------------- HELPERS ----------------####
section_start(){
	section="$1"
	shift
	echo -e "\e[0Ksection_start:`date +%s`:$section\r\e[0K$@"
}

section_end(){
	echo -e "\e[0Ksection_end:`date +%s`:$1\r\e[0K"
}

from_build(){
	if [[ -n `podman image ls -q -f reference=localhost/build-$CI_COMMIT_REF_NAME` ]]; then
		img=build-$CI_COMMIT_REF_NAME
	else
		img=registry.gitlab.com/ors-it-oss/spadowski:build-$CI_COMMIT_REF_NAME
	fi
}

####---------------- FUNCTIONS ----------------####
build(){
	clean &&
	wheel &&
	venv_pkg &&
	set -x
	pages build/pages
}

clean(){
	rm -rf $CI_PROJECT_DIR/{build,public,src/build} */__pycache__ src/*.egg-info
}

container(){
	cnt="$1"
	case $cnt in
		build) build_envs="BUILD_FROM_TAG FROM_SCRIPTS_TAG PIPS" ;;
		run) build_envs="RUN_FROM" ;;
		fat) df=build; FAT=very; build_envs="BUILD_FROM_TAG FROM_SCRIPTS_TAG FAT" ;;
		*) exit 1 ;;
	esac

	for arg in $build_envs; do
		build_args="$build_args${!arg:+ --build-arg "$arg=${!arg}"}"
	done

	if echo "$VERBOSE" | grep -q -e "all" -e "oci"; then
		build_args="$build_args --log-level debug"
	fi

	buildah bud $build_args \
		-v $PWD/src/requirements.txt:/build/src/requirements.txt:ro,z \
		-v $PWD/pages/requirements.txt:/build/pages/requirements.txt:ro,z \
		-t $cnt-$CI_COMMIT_REF_NAME -f cicd/${df:-$cnt}.Dockerfile .
}

pages(){
#	pip install src/ -r pages/requirements.txt
#	PATH="$PATH:~/.local/bin"
#	export PYTHONPATH=src/
	sphinx-build -b html pages ${1:-$CI_PROJECT_DIR/public}
}

push(){
	python -m twine upload \
		--repository-url https://gitlab.example.com/api/v4/projects/${CI_PROJECT_ID}/packages/pypi dist/*
}

venv(){
	venv_dir="${1:-/spadowski}"
	section_start venv "Compiling virtualenv in $venv_dir..."
	[[ -z "$DEVEL" ]] && pipargs="--force-reinstall"

	python -m venv --system-site-packages --symlinks --without-pip "$venv_dir"
	$venv_dir/bin/python -m pip install $pipargs $CI_PROJECT_DIR/build/*.whl

	section_end venv
}

venv_pkg(){
	venv
	mv /spadowski $CI_PROJECT_DIR/build/spadowski || true
}

wheel(){
	export SETUPTOOLS_SCM_DEBUG=1
	section_start wheel "Compiling Wheel package..."
	[[ -d $CI_PROJECT_DIR/build/ ]] || mkdir -p $CI_PROJECT_DIR/build/
	python -m build --wheel ${DEVEL:+--sdist} --outdir $CI_PROJECT_DIR/build/ src/
	section_end wheel
}

#tests(){
#}

workspace(){
	if [[ -n "$RUN" ]]; then
		img=gcr.io/distroless/python3-debian10:debug
		pargs="--entrypoint=sh"
	elif [[ -n "$FAT" ]]; then
		img=fat-$CI_COMMIT_REF_NAME
	else
		from_build
	fi
	name="spadowski_$(date +%s)"
	[[ -z "$DEVEL" ]] && pargs="$pargs --userns=keep-id --read-only"
	podman run --rm -ti --name $name ${pargs} \
		--security-opt label=disable \
		--workdir /src \
		--tmpfs /run:noexec --tmpfs /tmp \
		--tmpfs /spadowski --tmpfs /home/.local \
		--env SPADOWSKI_GITLAB --env SPADOWSKI_TOKEN \
		--env HOME=/home \
		--env TERM=xterm-256color \
		--volume $PWD:/src:z \
	$img "$@"
}


####---------------- CONTAINERIZED ----------------####
cnt_build(){
	FAT=1 workspace /src/cicd/run.sh build
	firefox file://$PWD/build/pages/index.html &
	container run
}

cnt_wheel(){
	workspace /src/cicd/run.sh wheel
}

cnt_venv(){
	workspace /src/cicd/run.sh venv_pkg
}

cnt_pages(){
	FAT=very workspace /src/cicd/run.sh pages "${@:-build/pages}"
}

####---------------- EXECUTION ----------------####
if [[ "$CI" != "true" ]]; then
	set -a
	CI_PROJECT_DIR=$PWD

	if [[ -d .git && $(which git) ]]; then
#		CI_COMMIT_REF_NAME="$(git branch --show-current)"
		CI_COMMIT_REF_NAME="$(git rev-parse --abbrev-ref HEAD)"
	fi
#	if which podman >/dev/null; then
	FROM_SCRIPTS_TAG=latest
	RUN_FROM=gcr.io/distroless/python3-debian10:debug
#	fi
	set +a
fi

if [[ -n "$1" ]]; then
	cmd=$1; shift
	case $cmd in
		*) $cmd "$@" ;;
	esac
fi
