local profiles = import 'profiles.libsonnet';
[{
	filters: {
		membership: true,
		group: "ors-it-oss/pequod"
	},
	attributes:
	 profiles.default.attributes + profiles.oss.attributes +
	 {
	 		container_registry_enabled: true,
			jobs_enabled: true,
			packages_enabled: false,
			snippets_access_level: 'disabled',
			wiki_access_level: 'disabled',
	 }
}]
