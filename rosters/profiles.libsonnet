/*
			container_registry_enabled: false,
			jobs_enabled: true,
			packages_enabled: false,

*/

{
default: {
	attributes: {
//			compliance_frameworks: ['gpdr'], TODO: nope
			auto_devops_enabled: false,
			ci_forward_deployment_enabled: false,
			emails_disabled: true,
			lfs_enabled: false,
			printing_merge_request_link_enabled: false,
			request_access_enabled: true,
//			requirements_enabled: false, TODO: broken
			security_and_compliance_enabled: true,
			service_desk_enabled: false,
			shared_runners_enabled: true,
	}
},
private: {
	attributes: {
		visibility: 'private',
		analytics_access_level: 'private',
		builds_access_level: 'private',
		issues_access_level: 'private',
		merge_requests_access_level: 'private',
		operations_access_level: 'private',
		pages_access_level: 'private',
//		public_jobs: false,  TODO: broken
		snippets_access_level: 'private',
		wiki_access_level: 'private'
	}
},
oss: {
	attributes: {
		visibility: 'public',
		analytics_access_level: 'enabled',
		builds_access_level: 'private',
		issues_access_level: 'enabled',
		merge_requests_access_level: 'enabled',
		operations_access_level: 'private',
		pages_access_level: 'public',
//		public_jobs: true,  TODO: broken
		snippets_access_level: 'enabled',
		wiki_access_level: 'enabled'
	}
},
archived: {
	attributes: self.default.attributes + {
		shared_runners_enabled: false
	},
//	archived: true, # TODO: Separate function
}
}
