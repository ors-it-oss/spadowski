local profiles = import 'profiles.libsonnet';
[{
	filters: {
		membership: true,
		group: "ors-it-oss/talend-components"
	},
	attributes:
		profiles.oss.attributes +	 profiles.archived.attributes + {
	 		container_registry_enabled: true,
			jobs_enabled: true,
			packages_enabled: false,
	 }
}]
