import setuptools

with open('README.md', 'r') as _fh:
  long_description = _fh.read()

# with open('LICENSE', 'r') as _fh:
#     license = _fh.read()

with open('requirements.txt') as _fh:
  install_requires = _fh.read().splitlines()

setuptools.setup(
  name='spadowski',
  version='0.1.0',
  entry_points={
    "console_scripts": [
      "spadowski = spadowski:__main__"
    ]
  },
  author='The Loeki',
  author_email='the.loeki+spadowski@gmail.com',
  description='A Gitlab janitor',
  install_requires=install_requires,
  # license=license,
  long_description=long_description,
  long_description_content_type='text/markdown',
  url='https://gitlab.com/ors-it-oss/spadowski',
  packages=setuptools.find_packages(),
  classifiers=[
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3 :: Only",
    "Development Status :: 4 - Beta",
    "License :: OSI Approved :: GNU Affero General Public License v3",
    "Operating System :: OS Independent",
    "Intended Audience :: System Administrators",
    "Intended Audience :: Developers",
    "Topic :: Software Development :: Build Tools",
    "Topic :: Software Development :: Quality Assurance",
    "Topic :: System :: Systems Administration",
    "Topic :: Utilities"
  ],
)